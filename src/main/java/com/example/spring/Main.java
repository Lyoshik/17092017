package com.example.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("example.xml");
        DataBase dataBase = (DataBase) applicationContext.getBean("clients");
        List<String> clients = dataBase.getClients().stream()
                                       .map(element -> "Id: " + element.getId() + " | Name: " + element.getName())
                                       .collect(Collectors.toList());
        clients.forEach(System.out::println);
    }

}
